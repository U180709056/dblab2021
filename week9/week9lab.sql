select * from customers;

update customers set country = replace(country, '\n', '');
update customers set city = replace(country, '\n', '');

create view mexicancustomers as 
select customerID, customername, contactname
from customers
where country = "Mexico";

select * from mexicancustomers;

select * 
from mexicancustomers join orders on mexicancustomers.customerID = orders.CustomerID;

create view productsbelowavg as 
select productid, productname, price
from products
where price < (select avg(price) from products);

#delete from orderdetails where productid = 5;
#truncate orderdetails;

delete from customers;

drop table customers;

